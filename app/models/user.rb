class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable, :confirmable
  has_and_belongs_to_many :roles
  validate :check_has_minimum_one_role
  has_many :rate_history_entries, dependent: :destroy
  has_many :transactions, dependent: :destroy
  has_one :balance, dependent: :destroy
  after_create :create_empty_balance
  
  def role?(role)
    return self.roles.find_by_name(role).try(:name) == role.to_s
  end

  def check_has_minimum_one_role
    if self.roles.empty?
      self.errors.add(:role, I18n.t('errors.messages.must_be_selected'))
    end
  end

  def create_empty_balance
    self.create_balance(amount: 0)
  end
end
