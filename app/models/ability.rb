class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user
    @user.roles.each do |role|
      if role.name == 'admin'
        admin(user)
      elsif role.name == 'customer'
        customer(user)
      end
    end
  end
  def admin(user)
    can :manage, :all
  end

  def customer(user)
    can :manage, :all
  end
end
