class Balance < ActiveRecord::Base
	validates :amount, :user, presence: true
	validates :amount, numericality: true
	belongs_to :user
end
