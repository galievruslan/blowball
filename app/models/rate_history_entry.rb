class RateHistoryEntry < ActiveRecord::Base
  validate :start, :user, :rate, presence: true
  validate :start_not_less_current_time, :on => :create
  validate :finish_not_less_start
  belongs_to :rate
  belongs_to :user

  def start_not_less_current_time
    if self.finish == nil and self.start < DateTime.now
      self.errors.add(:start, I18n.t('errors.messages.start_not_less_current_time'))
    end
  end

  def finish_not_less_start
    if self.finish
      if self.finish < self.start
        self.errors.add(:start, I18n.t('errors.messages.start_not_longer_finish'))
      end
    end
  end
end
