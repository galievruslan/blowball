class Transaction < ActiveRecord::Base
	validates :amount, :user, :transaction_type, presence: true
	validates :amount, numericality: {greater_than: 0}
  belongs_to :user
  after_save :update_balance
  after_destroy :edit_balance
  TYPES = {1 => I18n.t(:refill), 2 => I18n.t(:write_off)}

  validates_inclusion_of :transaction_type, :in => TYPES.keys, :message => "must be in #{TYPES.values.join ','}"

  def transaction_type_name
    TYPES[transaction_type]
  end

  def update_balance
  	current_balance = self.user.balance.amount
  	type = self.transaction_type
  	if type == 1
  		new_balance = current_balance + self.amount
  	elsif type == 2
  		new_balance = current_balance - self.amount
  	end
  	self.user.balance.update_attributes(amount: new_balance)
  end

  def edit_balance
  	current_balance = self.user.balance.amount
  	type = self.transaction_type
  	if type == 1
  		new_balance = current_balance - self.amount
  	elsif type == 2
  		new_balance = current_balance + self.amount
  	end
  	self.user.balance.update_attributes(amount: new_balance)
  end

end
