class RateHistoryEntriesController < ApplicationController
  load_and_authorize_resource

  def index
    @rates = Rate.all
    rates_is_null_finish = current_user.rate_history_entries.where("start <= ? and finish is null", DateTime.now)
    rates_is_not_null_finish = current_user.rate_history_entries.where("start <= ? and finish > ?", DateTime.now, DateTime.now)
    if rates_is_null_finish.count > 0
      @current_rate = rates_is_null_finish.first
    else
      @current_rate = rates_is_not_null_finish.first
      @next_rate = current_user.rate_history_entries.where("start > ? and finish is null", DateTime.now).first
    end   
  end

  def history
    @rate_history_entries = current_user.rate_history_entries.page(params[:page])
    rates_is_null_finish = current_user.rate_history_entries.where("start <= ? and finish is null", DateTime.now)
    rates_is_not_null_finish = current_user.rate_history_entries.where("start <= ? and finish > ?", DateTime.now, DateTime.now)
    if rates_is_null_finish.count > 0
      @current_rate = rates_is_null_finish.first
    else
        @current_rate = rates_is_not_null_finish.first
        @next_rate = current_user.rate_history_entries.where("start > ? and finish is null", DateTime.now).first
    end
  end

  def edit
    unless params[:rate_id]
      redirect_to rate_index_path, alert: t(:rate_not_selected)
      return
    end

    @next_rate = current_user.rate_history_entries.where("start > ? and finish is null", DateTime.now).first
    if @next_rate
      redirect_to rate_index_path, alert: t(:rate_has_changed)
      return
    end

    @rate = Rate.find(params[:rate_id])
    current_rate = current_user.rate_history_entries.last

    if current_rate and @rate == current_rate.rate
      redirect_to rate_index_path, alert: t(:selected_current_rate)
      return
    end

    if current_rate
      @rate_history_entry = current_rate
    else
      @rate_history_entry = RateHistoryEntry.new
    end
  end

  def cancel
    @next_rate = current_user.rate_history_entries.where("start > ? and finish is null", DateTime.now).first
    @current_rate = current_user.rate_history_entries.where("start <= ? and finish > ?", DateTime.now, DateTime.now).first
    if @next_rate 
      @next_rate.destroy
      if @current_rate
        @current_rate.update_attributes(finish: nil)
      end
      redirect_to rate_index_path, notice: t(:rate_change_canceled)      
    else
      redirect_to rate_index_path, alert: t(:rate_change_not_found)

    end
  end

  def update
    unless rate_history_entry_params[:rate_id]
      redirect_to rate_index_path, alert: t(:rate_not_selected)
      return
    end

    @next_rate = current_user.rate_history_entries.where("start > ? and finish is null", DateTime.now).first
    if @next_rate
      redirect_to rate_index_path, alert: t(:rate_has_changed, href: view_context.link_to(t(:rate_history_2), rate_history_path))
      return
    end

    @rate = Rate.find(rate_history_entry_params[:rate_id])
    if current_user.rate_history_entries.last
      @rate_history_entry = current_user.rate_history_entries.last
      @rate_history_entry.finish = rate_history_entry_params[:finish]

      if @rate_history_entry.save
        @new_rate = RateHistoryEntry.create(user: current_user, start: rate_history_entry_params[:finish], rate_id: rate_history_entry_params[:rate_id])
        redirect_to rate_index_path, notice: t(:rate_changed)
      else
        render :edit
      end
    else
      @rate_history_entry = RateHistoryEntry.new(user: current_user, start: rate_history_entry_params[:finish], rate_id: rate_history_entry_params[:rate_id])
      if @rate_history_entry.save
        redirect_to rate_index_path, notice: t(:rate_selected)
      else
        render :edit
      end
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def rate_history_entry_params
      params.require(:rate_history_entry).permit(:rate_id, :finish)
    end
end
