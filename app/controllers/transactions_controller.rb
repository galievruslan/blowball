class TransactionsController < ApplicationController
  load_and_authorize_resource
  before_action :set_user, except: [:refill_new, :refill_create, :history, :billing]
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]

  # GET /transactions
  # GET /transactions.json
  def index
    @search = @user.transactions.search(params[:q])
    @transactions = @search.result.page(params[:page])
  end

  # GET /transactions/history
  def history
    @search = current_user.transactions.search(params[:q])
    @transactions = @search.result.page(params[:page])
  end

  # GET /transactions/1
  # GET /transactions/1.json
  def show
  end

  # GET /transactions/new
  def new
    @transaction = @user.transactions.new
  end

  # GET /transactions/1/edit
  def edit
  end

  # GET /billing
  def billing 
    @user = current_user
  end

  # GET /transactions/refill
  def refill_new
    @transaction = current_user.transactions.new
  end

  # POST /transactions/refill
  def refill_create
    @transaction = current_user.transactions.new(transaction_params)
    @transaction.transaction_type = 1
    respond_to do |format|
      if @transaction.save
        format.html { redirect_to root_path, notice: t(:transaction_created) }
      else
        format.html { render :refill_new }
      end
    end
  end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = @user.transactions.new(transaction_params)

    respond_to do |format|
      if @transaction.save
        format.html { redirect_to user_transactions_path(@user), notice: t(:transaction_created) }
        format.json { render :show, status: :created, location: @transaction }
      else
        format.html { render :new }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    respond_to do |format|
      if @transaction.update(transaction_params)
        format.html { redirect_to user_transactions_path(@user), notice: t(:transaction_updated) }
        format.json { render :show, status: :ok, location: @transaction }
      else
        format.html { render :edit }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to user_transactions_path(@user), notice: t(:transaction_destroyed) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = @user.transactions.find(params[:id])
    end

    def set_user
      @user = User.find(params[:user_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.require(:transaction).permit(:amount, :transaction_type)
    end
end
