Rails.application.routes.draw do

  devise_for :users
  root 'pages#index'
  get 'bali', to: 'pages#bali'
  get 'rate_list', to: 'rate_history_entries#index', as: 'rate_index'
  get 'rate_history', to: 'rate_history_entries#history'
  get 'rate/edit', to: 'rate_history_entries#edit'
  put 'rate/update', to: 'rate_history_entries#update'
  delete 'rate/cancel', to: 'rate_history_entries#cancel'
  get 'transactions/refill', to: 'transactions#refill_new'
  post 'transactions/refill', to: 'transactions#refill_create'
  get 'transactions/history', to: 'transactions#history'
  get 'billing', to: 'transactions#billing'

  resources :rates

  resources :users, path: 'members' do
    resources :transactions
    member do
      get :edit_password
      put :update_password
    end
  end
end
