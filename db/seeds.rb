# Create default root user
role_admin = Role.create(name: 'admin')
role_customer = Role.create(name: 'customer')

user_admin = User.create(email: 'admin@test.com', password: '423200', password_confirmation: '423200', confirmed_at: DateTime.now)
user_admin.roles << role_admin
user_admin.save