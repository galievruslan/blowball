class CreateRateHistoryEntries < ActiveRecord::Migration
  def change
    create_table :rate_history_entries do |t|
      t.references :rate, index: true
      t.references :user, index: true
      t.datetime :start
      t.datetime :finish

      t.timestamps
    end
  end
end
