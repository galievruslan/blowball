class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :user, index: true
      t.decimal :amount, precision: 8, scale: 2
      t.timestamps
    end
  end
end
