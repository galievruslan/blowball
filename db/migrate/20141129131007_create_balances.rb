class CreateBalances < ActiveRecord::Migration
  def change
    create_table :balances do |t|
      t.decimal :amount, precision: 8, scale: 2
      t.references :user, index: true
      t.timestamps
    end
  end
end
